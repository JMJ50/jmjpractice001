const router = require('express').Router();

router.post('/exam001', (req, res) => {
    // console.log(req.body );
    var result = [];

    function solution(input){
        var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi; //특수문자 정규식

        var returnValue = input.replace(regExp, ""); //특수문자에 해당하는건 빈값으로 대체.
        return returnValue;
    }

    for(var i = 0; i < req.body.length; i++){
        var arr = req.body[i];
        var input = arr.input; //배열 원소(객체)의 input

        var returnValue = solution(input); //특수문자 제외.
        var value = parseInt(returnValue); //특수문자 제외한 text를 숫자형으로 변환
    
        if(value){
            arr.output = (value);
        }else{
            arr.output = ('Not a Number');
        };
        result.push(arr);
    };
   res.json(result); //응답 보내기.

})



router.post('/exam002', (req, res) => {
    // console.log(req.body);
    var result = [];
    var quantity;
    
    for(var i = 0; i<req.body.length; i++){
        var arr = req.body[i]; //배열 원소(객체) -input
        var fixed = arr.input[0]; //배열의 index 0 값 = 고정 비용
        var variable = arr.input[1]; //배열의 index 1 값 = 가변 비용
        var price = arr.input[2]; //배열의 index 2 값 = 가격
        
        if(variable > price){
            quantity = -1;
        }else{
        var returnValue = fixed / (price - variable);
        var quantity = 1 + Math.floor(returnValue); // 손익분기점 넘는 수량식
    
    };
    arr.output = quantity; //배열 원소(객체)에 output 추가
    result.push(arr); };

    res.json(result);
})




router.post('/exam003', (req, res) => {
    // console.log(req.body);
    const fix = [6, 2, 4];
    function solution(input){
        var str = '';
        while(input > 0){
            var remain = input % 3;
            str = fix[remain] + str;
            if(input < 3) break;
            if(remain === 0){ //3의 배수.
                input = Math.floor((input / 3) - 1);
            } else{
                input = Math.floor(input / 3);
            };
        }
        return str;
    }

    var data = req.body;
    for(var i = 0; i < data.length; i++){
        var arg = data[i].input;
        data[i].output = solution(arg);       
    };
    res.json(data);
})


router.post('/exam004', (req, res) => { // 하노이의 탑

    var result = req.body;
    result.output = [];

    let hanoi = (n, from, temp, to) => {
        if(n===1) result.output.push([from, to]); //가장 작은 원판
        else {
            hanoi(n-1, from, to, temp);
            result.output.push([from, to]);
            hanoi(n-1, temp, from, to);
        }
    }
    hanoi(result.input, 1, 2, 3);

    res.json(result);
})


router.post('/exam005', (req, res) => { //선택정렬

    var result = {};
    var curValue; //현재 값을 넣어둘 변수.
    var minIndex; //최소값의 index(2번째 for문 돌 때, 비교해서 작은 값으로 계속 update 됨)
    
    function solution(input){
        for(var i = 0; i < input.length; i++){
            minIndex = i;

            for(var idx = i + 1; idx < input.length; idx++){
                // 계속 비교하여, 최소값을 구함
                if(input[minIndex] > input[idx]) minIndex = idx;  
            };
            
            //SWAP 작업
            curValue = input[i]; //원래 값을 빼둠.
            input[i] = input[minIndex]; //현재 index = 최소값 index 값으로 바꿈.
            input[minIndex] = curValue; //최소값을 가졌던 index = 빼두었던 원래 값
        };
        return input;
    };

    result.result = solution(req.body.input);
    res.json(result);
})

router.post('/exam006', (req, res) => { //삽입정렬
    var result = {};

    function solution(input){
        var insertIndex;
        for(var i = 1; i < input.length; i++){
            var key = input[i];
            insertIndex = i;

            for(var idx = i - 1; idx >= 0; idx--){
                if(input[insertIndex] < input[idx]){
                    insertIndex = idx; //최종 이동할 자리의 값을 찾기 위해 update
                    input[idx + 1] = input[idx]; //대상보다 큰 값은 뒤로 보냄.
                }else{
                    break;
                };

                input[insertIndex] = key; //최종 이동할 자리에 key값을 넣음.
            };
        };
        return input;
    }

    result.result = solution(req.body.input);
    res.json(result);
})


router.post('/exam007', (req, res) => { //버블정렬
    var result = {};

    function solution(input){
        for(var cnt = input.length; cnt > 0; cnt--){ //cnt = 반복 횟수
            var i = 0; //반복할 때마다, 시작 index
            for(var rightIdx = i + 1; rightIdx < cnt; rightIdx++){
                if(input[i] > input[rightIdx]){
                    //swap
                    var temp = input[i];
                    input[i] = input[rightIdx];
                    input[rightIdx] = temp;
                    
                    i++; // 앞으로 이동했으니, 대상의 index도 update

                }else if(input[i] < input[rightIdx]){
                    i = rightIdx; //대상이 더 큰 수를 만나면, 더 큰 수가 대상이 됨
                };
            };
        };
        return input;
    }

    result.result = solution(req.body.input);
    res.json(result);
})




router.post('/exam008', (req, res) => { //퀵정렬 - in place 안정된 정렬 
    var result = {};
    var arr = [];

    function solution(input) {
        var pivot = input[0];
        var left = [];
        var right = [];

        if (input.length > 1) {
            for (var idx = 1; idx < input.length; idx++) {
                if (input[idx] < pivot) left.push(input[idx]);
                else if (input[idx] > pivot) {
                    right.push(input[idx]);
                };
            };
            return arr.concat(solution(left), pivot, solution(right));
        } else {
            return input;
        };

    };

    result.result = solution(req.body.input);
    res.json(result);
})



router.post('/exam009', (req, res) => { 
    var result = {};
    var arr = [];
    var temp;

    function solution(input){

        var pivotIndex = Math.floor(input.length / 2);
        if (input.length % 2 === 0) pivotIndex -= 1;
        var pivot = input[pivotIndex];
        var low = 0;
        var high = Math.floor(input.length - 1);
        var left = [];
        var right = [];
        var resultLeft = [];
        var resultRight = [];

        if (input.length > 1) {
            console.log(` low ${low} high ${high}`)
            while (low < high) { // low와 high가 엇갈리는 시점에는 pivot보다 작은, pivot보다 큰 것대로 배치함.
                //교환 할 필요 없으면, 이동
                while(input[low] < pivot){
                    low++;
                };
                while(input[high] > pivot){
                    high--;
                };

                //교환 
                if (low <= high) {
                    temp = input[high];
                    input[high] = input[low];
                    input[low] = temp;

                    low++;
                    high--;
                };
            };
            
            left = input.slice(0, low);
            right = input.slice( low , input.length);
            console.log(`left [${left}] pivot [${pivot} right [${right}]]`)
            if(left.length > 0) resultLeft = solution(left);
            if(right.length > 0) resultRight = solution(right);

            return arr.concat(resultLeft, resultRight);
         
        } else {
            return input;
        };
    };
    result.result = solution(req.body.input);
    res.json(result);
})




module.exports = router;