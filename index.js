const express = require('express'); //express 개체 생성
const app = express(); // app에 바인딩
const bodyParser = require('body-parser');

const apiRouter = require('./routes/api.route');

app.use(bodyParser.json());
app.use('/', apiRouter);

const port = process.env.PORT || 5000; //port 생성되어있으면 그것 사용 없으면 5000번 사용
app.listen( port, () => {
    console.log(`Server Start Port ${port}`);
}) //app listen으로 포트 지정.